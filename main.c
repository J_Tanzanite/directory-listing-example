#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>

void print_size(uint64_t bytes)
{
	char ctype[5] = "BKMG";
	int type = 0; /*0 = bytes, 1 = kb, 2 = mb, 3 = gb*/

	while (bytes >= 1024 && type < 3) {
		bytes /= 1024;
		type++;
	}

	printf("%4" PRIu64, bytes);
	printf("%c%-2s", ctype[type], ((type) ? "B" : ""));
}

void print_file_info(char *dir, char *file)
{
	/* +1 for NULL ternimator, and +1 for forwardslash */
	int size = strlen(dir) + strlen(file) + 2;
	char *path = malloc(size);
	struct stat bstat;
	char isdir = 0;

	snprintf(path, size, "%s/%s", dir, file);

	if (stat(path, &bstat) == -1) {
		fprintf(stderr, "Unable to access file info for %s\n", path);
		goto exit;
	}

	switch (bstat.st_mode & S_IFMT) {
	case S_IFSOCK: putchar('s'); break;
	case S_IFLNK: putchar('l'); break;
	case S_IFREG: putchar('f'); break;
	case S_IFBLK: putchar('b'); break;
	case S_IFDIR: putchar('d'); isdir = 1; break;
	case S_IFCHR: putchar('c'); break;
	case S_IFIFO: putchar('p'); break;
	default: putchar('u'); break;
	}

	printf(" %c%c%c%c%c%c%c%c%c  ",
		((bstat.st_mode & S_IRUSR) ? 'r' : '-'),
		((bstat.st_mode & S_IWUSR) ? 'w' : '-'),
		((bstat.st_mode & S_IXUSR) ? 'x' : '-'),
		((bstat.st_mode & S_IRGRP) ? 'r' : '-'),
		((bstat.st_mode & S_IWGRP) ? 'w' : '-'),
		((bstat.st_mode & S_IXGRP) ? 'x' : '-'),
		((bstat.st_mode & S_IROTH) ? 'r' : '-'),
		((bstat.st_mode & S_IWOTH) ? 'w' : '-'),
		((bstat.st_mode & S_IXOTH) ? 'x' : '-'));

	print_size((uint64_t) bstat.st_size);
	printf(" %s%s\n", file, ((isdir) ? "/" : ""));

exit:	free(path);
}

int print_directory(char *directory)
{
	DIR *dir;
	struct dirent *dirent;
	char *path;

	dir = opendir(directory);

	if (!dir) {
		fprintf(stderr, "Unable to open directory \"%s\"\n", directory);
		return 0;
	}

	path = realpath(directory, NULL);

	printf("Directory listing for \"%s\"\n", path);

	while ((dirent = readdir(dir))) {
		if (!strcmp(".", dirent->d_name)
			|| !strcmp("..", dirent->d_name))
			continue;

		print_file_info(path, dirent->d_name);
	}

	closedir(dir);
	free(path);

	return 1;
}

int main(int argc, char **argv)
{
	if (argc < 2)
		return !print_directory(".");

	for (int i = 1; i < argc; i++) {
		if (!print_directory(argv[i]))
			return 1;

		putchar('\n');
	}

	return 0;
}
